#ifndef CPP_PAD_MQTTSERVICE_H
#define CPP_PAD_MQTTSERVICE_H

#include <QtCore/QMap>
#include <QtMqtt/QMqttClient>
#include <QtMqtt/QMqttSubscription>

class MqttService : public QMqttClient {
Q_OBJECT
public:
    MqttService(QObject *parent = nullptr);
    Q_INVOKABLE QString mqttRequest(const QString &edgeUrl, const QString &requestMode, const QString &body);
    Q_INVOKABLE void updatedConnect(const QString &json);
signals:
    void setupGateway(const QString &mqttMsgText);
public slots:

    void handleMessage(const QMqttMessage &msg);

private:
    Q_DISABLE_COPY(MqttService)
    QString IP = "ip";
    QString USER = "user";
    QString PASSWD = "passwd";
    QString PORTS = "ports";
    QString TOPIC = "topic";
    QString SESSION_ID = "sessionId";
    QString TOPIC_PAD_DATA_REQ = "v1/pad/%1/request";
    QString TOPIC_PAD_DATA_RES = "v1/pad/%1/response";
    QString PAD_ID = "909A53BBB66A4F3318E9E243EF4C1001";
};

#endif
