import QtQuick 2.15
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.VirtualKeyboard

ApplicationWindow {

    id: root
    width: 1280
    height: 720
    visible: true
    title: "Mqtt Apifox"
    color: "transparent"

    Rectangle{
        anchors.fill: parent
        color: "transparent"
        Image {
            id: background
            source: "./images/background.png"
            anchors.fill: parent
        }

    }
    signal mqttRequest(string edgeUrl,string requestMode,string body)



    Loader{
        id:myLoader
        anchors.centerIn: parent
        anchors.fill:parent
        // 弹出的界面都居中显示
    }
    Component{
        id:qarams
        Qarams{}
    }
    Component{
        id:postman
        Postman{}
    }

    Component.onCompleted: {
        myLoader.sourceComponent = postman
    }

}
