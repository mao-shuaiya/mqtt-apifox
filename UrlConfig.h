#ifndef CPP_PAD_URLCONFIG_H
#define CPP_PAD_URLCONFIG_H

/**
 * url路径配置文件
 *
 * @author 田杰
 * @version 2022-03-31
 */

class UrlConfig {
public:

    constexpr static int URL_SETUP_GATEWAY_VALUE = 8; // /setup/gateway

    constexpr static int URL_MQTT_MSG_VALUE = 12; // /mqtt/msgs

    constexpr static int URL_SENSOR_VALUE = 14; // /sensors

    constexpr static int URL_SENSOR_TYPE_VALUE = 18; // /sensor/types

    constexpr static int URL_LOGIN_VALUE = 20; // /login/logins

    constexpr static int URL_PAD_USER_VALUE = 30; // /pad/users

    constexpr static int URL_JILIN_INDEX_VALUE = 32; // /jinlin/indexs

    constexpr static int URL_PAD_ALARM_VALUE = 33;  // /pad/alarms

    constexpr static int URL_MQTT_CONFIG_VALUE = 34; // /mqtt/configs

    constexpr static int URL_PROJECT_VALUE = 35;  // projects

};

#endif