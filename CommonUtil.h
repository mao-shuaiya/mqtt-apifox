#ifndef CPP_PAD_COMMONUTIL_H
#define CPP_PAD_COMMONUTIL_H
#include <QObject>
typedef unsigned char BYTE;
#define RTP_DATA_LENGTH 2048

using namespace std;

class CommonUtil : public QObject
{
    Q_OBJECT
public:
    CommonUtil(QObject *parent = nullptr);
    ~CommonUtil();

    /**
     * 16进制字符串转字节数组
     *
     * @param szHex 十六进制字符串
     * @return 字节码
     */
    static void hexStringToBytes(const std::string &hex, BYTE *bytes);

    /**
     * 二进制后3位转int
     *
     * @param bytes 流
     * @return int数据
     */
    static int byteLast3ToInt(const QByteArray &input);

    /**
     * 字节转int
     *
     * @param input 字节数据
     * @return int数据
     */
    static int byteToInt(const QByteArray &input);
};
#endif