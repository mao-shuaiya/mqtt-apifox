import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.VirtualKeyboard

Rectangle{
    width: parent.width
    height: parent.height
    Rectangle{
        id:setPage
        width: parent.width
        height: parent.height
        color: "#ffffff"

        property var edgeUrl:usrInput.text
        property var requestMode:method.text
        property var body:requestText.text
        Item {
            id: model
            property string selected: "get"
        }

    //        请求方式
        Rectangle{
            width: parent.width-170
            x:20
            y:40
            radius: 15
            height: 70
            color: "#f2f2f2"
            Text {
                id: method
                text: model.selected
                color: "#616161"
                font.pixelSize: 21
                y:(parent.height-height)/2
                x:25
                font.bold: true
            }
            Image {
                source: "./images/select.png"
                y:(parent.height-height)/2
                x:xian.x-width-18
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        methods.visible = !methods.visible
                    }
                }
            }
            TextField {
                id: usrInput
                placeholderText: qsTr("Enter request URL")
                font.pixelSize: 16
                text: ""
                x:180
                width: parent.width*0.5
                height: parent.height
                placeholderTextColor:'#bdbdbd'
                color: "#333333"
                opacity: 0.8
                activeFocusOnPress:true
                selectByMouse: true
                verticalAlignment: Text.AlignVCenter
                background: Rectangle{
                    radius: 10
                    color: "transparent"
                }
            }
            Rectangle{
                id:xian
                width: 1
                height: parent.height*0.85
                color: "#5d5e5f"
                opacity: 0.2
                x:150
                y:(parent.height-height)/2
            }
        }
        Rectangle{
            width: 120
            height: 70
            x:parent.width-width-20
            color: "#097bed"
            y:40
            radius: 15
            Text {
                text: "Send"
                color: "#FFFFFF"
                font.pixelSize: 18
                anchors.centerIn: parent
                font.bold: true
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    mqtt.mqttRequest(setPage.edgeUrl,setPage.requestMode,setPage.body)
                }
            }
        }

        Text {
            id: ip
            text: "首次使用，请先配置Mqtt参数并重启"
            font.pixelSize: 15
            color: "#333333"
            x:(parent.width-width)/2
            y:110+(40-height)/2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    myLoader.sourceComponent = qarams
                }
            }
        }

        Rectangle{
            id:params
            width: parent.width
            height: 40
            color: "#fafafa"
            y:150
            Text {
                text: "Request parameters"
                y:(parent.height-height)/2
                x:15
                color: "#999999"

            }
            Rectangle{
                id:ipInput
                visible: false
                width: 150
                height: 30
                anchors.centerIn: parent
                TextField {
                    id: ipText
                    text: ""
                    placeholderText: qsTr("请输入IP")
                    font.pixelSize: 15
                    color: "#333333"
                }
            }
            Rectangle{
                id:ipSave
                visible: false
                width: 30
                height: 30
                color: "transparent"
                y:(parent.height-height)/2
                x:ipInput.x+ipInput.width
                Text {
                    text: "保存"
                    color: "blue"
                    font.pixelSize: 15
                    anchors.centerIn: parent
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        mqtt.updatedIp(ipText.text)
                        ipInput.visible = false
                        ipSave.visible = false
                    }
                }
            }
            Rectangle{
                width: parent.width
                height: 1
                color: "#333333"
                opacity: 0.15
                anchors.top: parent.top
            }
            Rectangle{
                width: parent.width
                height: 1
                color: "#333333"
                opacity: 0.15
                anchors.bottom: parent.bottom
            }
        }
    //        请求报文
        Rectangle{
            id:body
            width: parent.width
            height: 220
            y:params.y+params.height
            color: "#ffffff"
            Rectangle{
                width: parent.width-30
                height: parent.height-30
                anchors.centerIn: parent
                Flickable {
                    id: flick

                    width: parent.width
                    height: parent.height
//                    contentWidth: edit.paintedWidth
//                    contentHeight: edit.paintedHeight
                    clip: true

                     function ensureVisible(r)
                     {
                         if (contentX >= r.x)
                             contentX = r.x;
                         else if (contentX+width <= r.x+r.width)
                             contentX = r.x+r.width-width;
                         if (contentY >= r.y)
                             contentY = r.y;
                         else if (contentY+height <= r.y+r.height)
                             contentY = r.y+r.height-height;
                     }

                     TextEdit {
                         id: requestText
                         width: flick.width
                         focus: true
                         font.pixelSize: 16
                         wrapMode: TextEdit.Wrap
                         onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                     }
                 }

            }

        }
        Rectangle{
            id:huifu
            width: parent.width
            height: 40
            color: "#fafafa"
            y:body.y+body.height

            Text {
                text: "Response body"
                y:(parent.height-height)/2
                x:15
                color: "#999999"

            }
            Rectangle{
                width: parent.width
                height: 1
                color: "#333333"
                opacity: 0.15
                anchors.top: parent.top
            }
            Rectangle{
                width: parent.width
                height: 1
                color: "#333333"
                opacity: 0.15
                anchors.bottom: parent.bottom
            }
        }

    //        报文
        Rectangle{
            id:response
            width: parent.width
            height: 310
            y:huifu.y+huifu.height
            color: "#ffffff"
            Rectangle{
                width: parent.width-30
                height: parent.height-30
                anchors.centerIn: parent
                Flickable {
                    id: flick2

                    width: parent.width
                    height: parent.height
//                    contentWidth: edit.paintedWidth
//                    contentHeight: edit.paintedHeight
                    clip: true

                     function ensureVisible(r)
                     {
                         if (contentX >= r.x)
                             contentX = r.x;
                         else if (contentX+width <= r.x+r.width)
                             contentX = r.x+r.width-width;
                         if (contentY >= r.y)
                             contentY = r.y;
                         else if (contentY+height <= r.y+r.height)
                             contentY = r.y+r.height-height;
                     }

                     TextEdit {
                         id: aaaaaa
                         text: ""
                         width: flick.width
                         focus: true
                         font.pixelSize: 16
                         wrapMode: TextEdit.Wrap
                         onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                     }
                 }

            }

        }



    }

    Rectangle{
        id:methods
        width: 150
        height: 400
        visible: false
        color: "transparent"
        border.color: "#333333"
        x:20
        y:110
        Column{
            spacing: 0
            anchors.fill: parent
            Repeater{
                id:repeat
                model: methodsSelect
                property var methodsSelect: ["get","post","put","patch","delete","copy","head","link","unlink","purge","view"]
                Rectangle{
                    id:aa
                    width: parent.width
                    height: 40
                    Text {
                        id: name
                        text: repeat.methodsSelect[index]
                        anchors.centerIn: parent
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            method.text = name.text
                            methods.visible = !methods.visible
                        }

                        hoverEnabled: true //默认是false
                        onEntered: {
                            aa.color="#333333"
                            name.color = "#FFFFFF"
                        }
                        onExited: {
                            aa.color = "#FFFFFF"
                            name.color = "#333333"
                        }

                    }

                }
            }
        }
    }

    Component.onCompleted: {
        mqtt.onSetupGateway.connect(receive)
    }
    function receive(mqttMsgText){
    //        var res = JSON.parse(mqttMsgText)
        aaaaaa.text = mqttMsgText
    }
}

