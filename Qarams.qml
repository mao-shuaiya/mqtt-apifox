import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.VirtualKeyboard
Rectangle{
    width: parent.width
    height: parent.height
    color: "transparent"
    Text {
        id: title
        text: qsTr("Mqtt配置")
        color: "#FFFFFF"
        font.pixelSize: 25
        x:(parent.width-width)/2
        y:40
    }

    Rectangle{
        width: 400
        height: parent.height*0.6
        y:parent.height*0.4
        color: "transparent"
        anchors.centerIn: parent


        Column{
            anchors.fill: parent
            spacing: 20
            Rectangle{
                id:username
                width: 400
                height: 60
                radius: 32
                color: "transparent"
                Rectangle{
                    anchors.fill: parent
                    color: "#FBFBFB"
                    opacity: 0.05
                    radius: 32
                }
                TextField {
                    id: usernameInput
                    placeholderText: qsTr("请输入Mosquitto用户名")
                    font.pixelSize: 17
                    text: ""
                    x:40
                    width: 200
                    height: parent.height
                    placeholderTextColor:'#61778B'
                    color: "#FFFFFF"
                    opacity: 0.8
                    activeFocusOnPress:true
                    selectByMouse: true
                    verticalAlignment: Text.AlignVCenter
                    background: Rectangle{
                        radius: 10
                        color: "transparent"
                    }
                }
            }
            Rectangle{
                id:password
                width: 400
                height: 60
                radius: 32
                color: "transparent"
                Rectangle{
                    anchors.fill: parent
                    color: "#FBFBFB"
                    opacity: 0.05
                    radius: 32
                }
                TextField {
                    id: passwordInput
                    placeholderText: qsTr("请输入Mosquitto密码")
                    font.pixelSize: 17
                    text: ""
                    x:40
                    width: 200
                    height: parent.height
                    placeholderTextColor:'#61778B'
                    color: "#FFFFFF"
                    opacity: 0.8
                    activeFocusOnPress:true
                    selectByMouse: true
                    verticalAlignment: Text.AlignVCenter
                    background: Rectangle{
                        radius: 10
                        color: "transparent"
                    }
                }
            }
            Rectangle{
                id:ip
                width: 400
                height: 60
                radius: 32
                color: "transparent"
                Rectangle{
                    anchors.fill: parent
                    color: "#FBFBFB"
                    opacity: 0.05
                    radius: 32
                }
                TextField {
                    id: ipInput
                    placeholderText: qsTr("请输入IP地址")
                    font.pixelSize: 17
                    text: ""
                    x:40
                    width: 200
                    height: parent.height
                    placeholderTextColor:'#61778B'
                    color: "#FFFFFF"
                    opacity: 0.8
                    activeFocusOnPress:true
                    selectByMouse: true
                    verticalAlignment: Text.AlignVCenter
                    background: Rectangle{
                        radius: 10
                        color: "transparent"
                    }
                }
            }
            Rectangle{
                id:port
                width: 400
                height: 60
                radius: 32
                color: "transparent"
                Rectangle{
                    anchors.fill: parent
                    color: "#FBFBFB"
                    opacity: 0.05
                    radius: 32
                }
                TextField {
                    id: portInput
                    placeholderText: qsTr("请输入IP端口号")
                    font.pixelSize: 17
                    text: ""
                    x:40
                    width: 200
                    height: parent.height
                    placeholderTextColor:'#61778B'
                    color: "#FFFFFF"
                    opacity: 0.8
                    activeFocusOnPress:true
                    selectByMouse: true
                    verticalAlignment: Text.AlignVCenter
                    background: Rectangle{
                        radius: 10
                        color: "transparent"
                    }
                }
            }
            Rectangle{
                id:topic
                width: 400
                height: 60
                radius: 32
                color: "transparent"
                Rectangle{
                    anchors.fill: parent
                    color: "#FBFBFB"
                    opacity: 0.05
                    radius: 32
                }
                TextField {
                    id: topicInput
                    placeholderText: qsTr("请输入topic")
                    font.pixelSize: 17
                    text: ""
                    x:40
                    width: 200
                    height: parent.height
                    placeholderTextColor:'#61778B'
                    color: "#FFFFFF"
                    opacity: 0.8
                    activeFocusOnPress:true
                    selectByMouse: true
                    verticalAlignment: Text.AlignVCenter
                    background: Rectangle{
                        radius: 10
                        color: "transparent"
                    }
                }
            }
            Rectangle{
                width: 400
                height: 60
                color: "transparent"

                Rectangle{
                    width: 400
                    height: 60
                    radius: 32
                    anchors.centerIn: parent
                    Text {
                        id: name
                        text: "保存设置并使用"
                        color: "#333333"
                        font.pixelSize: 18
                        anchors.centerIn: parent
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {

                            let parmas = {
                                ip:ipInput.text,
                                user:usernameInput.text,
                                passwd:passwordInput.text,
                                ports:portInput.text,
                                topic:topicInput.text
                            }

                        //      接口数据封装进res并转为json
                            mqtt.updatedConnect(JSON.stringify(parmas))
                            myLoader.sourceComponent = postman
                        }
                    }
                }
            }

        }
    }







}
