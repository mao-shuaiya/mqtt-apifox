#include "MqttService.h"
#include "MsgExtern.h"
#include <QUuid>
#include <sys/procfs.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QApplication>
#include <fstream>
#include <QFile>
#include <QMessageBox>

using namespace std;


MqttService::MqttService(QObject *parent) : QMqttClient(parent) {
    QFile file("mosquitto.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) { //以以只读方式打开文本文件
        printf("read :open mosquitto.txt error \n");
        return;
    }
    QByteArray allArray = file.readAll();
    QString mqttConfig = QString(allArray);
    file.close();
    QJsonParseError jsonError;
    QJsonDocument doc;
    QJsonValue value;
    QJsonObject object;
    doc = QJsonDocument::fromJson(mqttConfig.toLatin1(), &jsonError);
    if (!doc.isNull() && jsonError.error == QJsonParseError::NoError) {
        if (doc.isObject()) {
            object = doc.object();
            if (object.contains(IP)) {
                value = object.value(IP);
                if (value.isString()) {
                    ip = value.toString();
                }
            }
            if (object.contains(USER)) {
                value = object.value(USER);
                if (value.isString()) {
                    user = value.toString();
                }
            }
            if (object.contains(PASSWD)) {
                value = object.value(PASSWD);
                if (value.isString()) {
                    passwd = value.toString();
                }
            }
            if (object.contains(PORTS)) {
                value = object.value(PORTS);
                if (value.isString()) {
                    ports = value.toString();
                }
            }
            if (object.contains(TOPIC)) {
                value = object.value(TOPIC);
                if (value.isString()) {
                    topics = value.toString();
                }
            }
        }
    }
    this->setHostname(ip);
    this->setUsername(user);
    this->setPassword(passwd);
    this->setPort(ports.toInt());
    this->setWillTopic(topics);
    this->connectToHost();
}

void MqttService::updatedConnect(const QString &json) {
    QFile file("mosquitto.txt");
    //不存在创建，存在则打开
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))//以只写方式打开
    {
        printf("write :open mosquitto.txt error \n");
        return;
    }
    file.write(json.toUtf8());
    file.close();
}


QString MqttService::mqttRequest(const QString &edgeUrl, const QString &requestMode, const QString &body) {
    if (isSubscribe == 0) {
        QString topic = QString(TOPIC_PAD_DATA_RES).arg(MqttService::PAD_ID);
        auto sub = QMqttClient::subscribe(topic, 0);
        connect(sub, &QMqttSubscription::messageReceived, this, &MqttService::handleMessage);
    }
    struct timeval tv = {0};
    gettimeofday(&tv, nullptr);
    QString createTime = QString::number((tv.tv_sec * 1000 + tv.tv_usec / 1000));
    QString topic = QString(TOPIC_PAD_DATA_REQ).arg(MqttService::PAD_ID);
    QString mid = QUuid::createUuid().toString();
    char sessionIdGet[33];
    sessionIdGet[0] = '\0';
    QString json;
    json.append(R"({"mid":")");
    json.append(mid);
    json.append(R"(","resStatus":0,"url":")");
    json.append(edgeUrl);
    json.append(R"(","requestMode":")");
    json.append(requestMode);
    json.append(R"(","body":)");
    json.append(body);
    json.append(R"(,"padId":")");
    json.append(MqttService::PAD_ID);
    json.append(R"(","gatewayId":")");
    json.append("0");
    json.append(R"(","createdTime":)");
    json.append(createTime);
    json.append(R"(,"updatedTime":)");
    json.append(createTime);
    json.append(R"(,"sessionId":")");
    ifstream iFile;
    iFile.open("sessionId.txt", ios::in);
    iFile >> sessionIdGet;
    iFile.close();
    sessionId = QString(QLatin1String(sessionIdGet));
    json.append(sessionId);
    json.append(R"("})");
//    printf("json-------------------------------------- %s\n", json.toStdString().data());
    QMqttClient::publish(QMqttTopicName(topic), json.toUtf8(), 0, false);
    return "200";
}

void MqttService::handleMessage(const QMqttMessage &msg) {
    if (isSubscribe == 0) {
        isSubscribe = 1;
    }
    mqttMsgText = msg.payload();
//    printf("handleMessage-------------------------------------- %s\n", mqttMsgText.toStdString().data());
    emit setupGateway(mqttMsgText);
}

