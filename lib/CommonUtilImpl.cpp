#include "CommonUtil.h"


CommonUtil::CommonUtil(QObject *parent) : QObject(parent) {
}

CommonUtil::~CommonUtil() {}

/**
 * 16进制字符串转字节数组
 *
 * @param szHex 十六进制字符串
 * @param bytes 输出的字节流
 * @return 字节码
 */
void CommonUtil::hexStringToBytes(const std::string &hex, BYTE *bytes) {
    int byteLen = (int) hex.length() / 2;
    std::string strByte;
    unsigned int n;
    for (int i = 0; i < byteLen; i++) {
        strByte = hex.substr(i * 2, 2);
        sscanf(strByte.c_str(), "%x", &n);
        bytes[i] = n;
    }
}

/**
 * 二进制后3位转int
 *
 * @param input 流
 * @return int数据
 */
int CommonUtil::byteLast3ToInt(const QByteArray &input) {
    bool ok;
    int result = input.toHex().toInt(&ok, 16);
    return ok ? result & 7 : 0;
}

/**
 * 字节转int
 *
 * @param input 字节数据
 * @return int数据
 */
int CommonUtil::byteToInt(const QByteArray &input) {
    bool ok;
    int result = input.toHex().toInt(&ok, 16);
    return ok ? result : 0;
}